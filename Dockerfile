FROM debian:stretch

WORKDIR /poller-os

RUN apt-get update
RUN apt-get install -y \
    gawk wget git-core diffstat unzip texinfo gcc-multilib \
    build-essential chrpath socat cpio python python3 python3-pip python3-pexpect \
    xz-utils debianutils iputils-ping python3-git python3-jinja2 libegl1-mesa libsdl1.2-dev \
    xterm

RUN wget http://downloads.yoctoproject.org/releases/yocto/yocto-2.7/buildtools/x86_64-buildtools-nativesdk-standalone-2.7.sh
RUN chmod 777 ./x86_64-buildtools-nativesdk-standalone-2.7.sh && sh ./x86_64-buildtools-nativesdk-standalone-2.7.sh

RUN git clone git://git.yoctoproject.org/poky
RUN git --git-dir=./poky/.git checkout tags/yocto-2.7 -b my-yocto-2.7

RUN chown -R 1000 .

COPY ./build.sh .

ENTRYPOINT [ "bash", "./build.sh" ]