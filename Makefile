all: run

build:
	docker build -t registry.gitlab.com/automac/poller-os .

push: build
	docker push registry.gitlab.com/automac/poller-os

run:
	docker run -u 1000 registry.gitlab.com/automac/poller-os