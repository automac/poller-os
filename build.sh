#!/bin/bash

source /opt/poky/2.7/environment-setup-x86_64-pokysdk-linux
source /poller-os/poky/oe-init-build-env

cd /poller-os/poky
# https://www.yoctoproject.org/docs/latest/brief-yoctoprojectqs/brief-yoctoprojectqs.html#packages
# https://www.balena.io/docs/reference/OS/overview/2.x/
# https://github.com/agherzan/meta-raspberrypi
# git clone https://github.com/balena-os/meta-balena.git

bitbake core-image-sato